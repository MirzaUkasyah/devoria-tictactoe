package com.lgd.tictactoe

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.navigation
import androidx.navigation.compose.rememberNavController
import com.google.firebase.FirebaseApp
import com.lgd.tictactoe.core.navigation.Directions
import com.lgd.tictactoe.core.navigation.NavigationManager
import com.lgd.tictactoe.core.theme.TicTacToeTheme
import com.lgd.tictactoe.presentation.ui.GettingStartedComposable
import com.lgd.tictactoe.presentation.ui.PlayComposable
import com.lgd.tictactoe.presentation.viewmodel.TicTacToeViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    @Inject
    lateinit var navigationManager: NavigationManager

    @ExperimentalFoundationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FirebaseApp.initializeApp(this)
        setContent {
            TicTacToeTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    App(navigationManager)
                }
            }
        }
    }
}

@ExperimentalFoundationApi
@Composable
fun App(navigationManager: NavigationManager) {
    val navController = rememberNavController()
    navigationManager.commands.collectAsState().value.also { command ->
        if (command.destination.isNotEmpty()) {
            navController.navigate(command.destination)
        }
    }

    val viewModel: TicTacToeViewModel = hiltViewModel()

    NavHost(navController = navController, startDestination = Directions.Home.destination) {
        composable(Directions.Home.destination) { GettingStartedComposable(viewModel) }
        composable(Directions.Play.destination) { PlayComposable(viewModel) }
    }
}