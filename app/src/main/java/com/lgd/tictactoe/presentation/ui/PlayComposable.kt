package com.lgd.tictactoe.presentation.ui

import android.widget.Toast
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.constraintlayout.compose.ConstraintLayout
import com.lgd.tictactoe.core.entity.Room
import com.lgd.tictactoe.core.interfaces.ResultState
import com.lgd.tictactoe.core.widget.DialogComposable
import com.lgd.tictactoe.core.widget.SquareComposable
import com.lgd.tictactoe.presentation.viewmodel.TicTacToeViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@ExperimentalFoundationApi
@Composable
fun PlayComposable(viewModel: TicTacToeViewModel) {
    Scaffold(modifier = Modifier.fillMaxSize()) {
        val result = viewModel.result().collectAsState(initial = ResultState.Loading)

        when (result.value) {
            is ResultState.Error -> {
                Toast.makeText(LocalContext.current, "Error Load Room Data", Toast.LENGTH_LONG)
                    .show()
                viewModel.reset()
            }
            ResultState.Loading -> {
                Column(
                    modifier = Modifier.fillMaxSize(),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    CircularProgressIndicator()
                }
            }
            is ResultState.Success<*> -> {
                val room = (result.value as ResultState.Success<*>).data as Room
                viewModel.checkMatchStatus(room)
                ConstraintLayout(modifier = Modifier.fillMaxSize()) {

                    val (grid, turn) = createRefs()

                    Text(
                        modifier = Modifier.constrainAs(turn) {
                            top.linkTo(parent.top)
                            bottom.linkTo(grid.top)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        },
                        text = "${
                            room.players[room.activePlayer - 1].name
                        } Turn"
                    )

                    LazyVerticalGrid(
                        modifier = Modifier.constrainAs(grid) {
                            top.linkTo(parent.top)
                            bottom.linkTo(parent.bottom)
                        },
                        cells = GridCells.Fixed(3)
                    ) {
                        items(room.gameState.size) { index ->
                            SquareComposable(
                                value = room.gameState[index],
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .clickable {
                                        if (room.gameState[index] == 0 && room.activePlayer == viewModel.player.value.number?.toInt()) {
                                            viewModel.squareTap(room.gameState, index)
                                        }
                                    }
                            )
                        }
                    }

                    DialogComposable(message = room.status) {
                        viewModel.reset()
                    }
                }
            }
        }

    }
}

@ExperimentalFoundationApi
@Preview
@Composable
fun PlayComposablePreview() {
    Scaffold(modifier = Modifier.fillMaxSize()) {
        ConstraintLayout(modifier = Modifier.fillMaxSize()) {

            val (grid, turn) = createRefs()

            Text(
                modifier = Modifier.constrainAs(turn) {
                    top.linkTo(parent.top)
                    bottom.linkTo(grid.top)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                },
                text = "Player Turn"
            )

            LazyVerticalGrid(
                modifier = Modifier.constrainAs(grid) {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                },
                cells = GridCells.Fixed(3)
            ) {
                items(9) { index ->
                    SquareComposable(value = 0, modifier = Modifier.fillMaxWidth())
                }
            }
        }
    }
}