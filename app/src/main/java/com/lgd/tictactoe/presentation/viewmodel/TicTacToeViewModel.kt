package com.lgd.tictactoe.presentation.viewmodel

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.ktx.Firebase
import com.lgd.tictactoe.core.entity.Player
import com.lgd.tictactoe.core.entity.Room
import com.lgd.tictactoe.core.interfaces.ResultState
import com.lgd.tictactoe.core.navigation.Directions
import com.lgd.tictactoe.core.navigation.NavigationManager
import com.lgd.tictactoe.core.repositories.RealtimeDatabaseRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TicTacToeViewModel @Inject constructor(
    private val repository: RealtimeDatabaseRepository,
    private val navigationManager: NavigationManager
) :
    ViewModel() {
    val playerName = MutableStateFlow("")
    val roomId = MutableStateFlow("")
    val firebaseAnalytics = Firebase.analytics
    var flag = 0
    val player = MutableStateFlow(Player())

    var winPositions = arrayOf(
        intArrayOf(0, 1, 2),
        intArrayOf(3, 4, 5),
        intArrayOf(6, 7, 8),
        intArrayOf(0, 3, 6),
        intArrayOf(1, 4, 7),
        intArrayOf(2, 5, 8),
        intArrayOf(0, 4, 8),
        intArrayOf(2, 4, 6)
    )

    var counter = 0

    @ExperimentalCoroutinesApi
    fun result(): Flow<ResultState> {
       return repository.retrieveGameData(roomId.value)
    }

    fun checkMatchStatus(room: Room){
        val gameState = room.gameState
        for (winPosition in winPositions) {
            if (gameState[winPosition[0]] == gameState[winPosition[1]] &&
                gameState[winPosition[1]] == gameState[winPosition[2]] &&
                gameState[winPosition[0]] != 0
            ) {
                Log.e("HERE", player.value.name ?: "-")
                if(flag == 0) {
                    repository.updateGameStatus(
                        roomId.value,
                        "${player.value.name} has won"
                    )

                    firebaseAnalytics.logEvent("match_result", Bundle().apply {
                        putString("roomId", roomId.value)
                        putString("game_state", gameState.joinToString(","))
                        putString("winner", player.value.name)
                        putString("match_status", "WON")
                    })
                    flag = 1
                }

            }
        }

        if ((0 !in gameState.toIntArray()) && flag == 0) {
            repository.updateGameStatus(roomId.value, "Match Draw")
            firebaseAnalytics.logEvent("match_result", Bundle().apply {
                putString("roomId", roomId.value)
                putString("game_state", gameState.joinToString(","))
                putString("match_status", "DRAW")
            })
        }
    }

    @ExperimentalCoroutinesApi
    fun play() {
        viewModelScope.launch {
            repository.retrieveRoomList(roomId.value).collect {
                when (it) {
                    is ResultState.Error -> Log.e("Err", it.throwable.message ?: "--")
                    is ResultState.Success<*> -> {
                        val data = it.data as DocumentSnapshot
                        if (data.data.isNullOrEmpty()) {
                            val firstPlayer = Player(
                                name = playerName.value,
                                number = 1
                            )
                            player.value = firstPlayer
                            repository.setData(
                                roomId.value,
                                listOf(firstPlayer),
                                listOf(0, 0, 0, 0, 0, 0, 0, 0, 0),
                                1,
                                ""
                            )

                        } else {
                            val playerList = data.get("players") as List<Map<String, Any>>

                            val firstPlayer = Player(
                                name = playerList[0]["name"] as String?,
                                number = playerList[0]["number"] as Long?
                            )

                            val secondPlayer = Player(
                                name = playerName.value,
                                number = 2
                            )

                            firebaseAnalytics.logEvent("match_start", Bundle().apply {
                                putString("room_id", roomId.value)
                                putString("first_player", firstPlayer.name)
                                putString("second_player", secondPlayer.name)
                            })

                            player.value = secondPlayer
                            repository.addPlayer(
                                roomId.value,
                                listOf(firstPlayer, secondPlayer)
                            )
                        }
                        navigationManager.navigate(Directions.Play)
                    }
                }
            }
        }
    }

    fun squareTap(gameState: List<Int>, position: Int) {
        counter++

        val list = gameState.toMutableList()

        list[position] = player.value.number!!.toInt()
        repository.updateGameState(roomId.value, list)

        if (player.value.number!!.toInt() == 1) {
            repository.updateActivePlayer(roomId.value, 2)
        } else {
            repository.updateActivePlayer(roomId.value, 1)
        }

    }

    fun reset() {
        counter = 0
        flag = 0
        navigationManager.navigate(Directions.Home)
    }
}