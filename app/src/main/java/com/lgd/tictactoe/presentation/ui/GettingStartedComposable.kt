package com.lgd.tictactoe.presentation.ui

import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import com.lgd.tictactoe.R
import com.lgd.tictactoe.presentation.viewmodel.TicTacToeViewModel

@Composable
fun GettingStartedComposable(viewModel: TicTacToeViewModel) {
    Scaffold(
        modifier = Modifier.fillMaxSize()
    ) {
        val context = LocalContext.current

        ConstraintLayout(
            modifier = Modifier
                .fillMaxSize()
                .padding(horizontal = 16.dp, vertical = 32.dp)
        ) {
            val (button, textGroup, title) = createRefs()

            Text(
                modifier = Modifier.constrainAs(title) {
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                    bottom.linkTo(textGroup.top)
                },
                text = "TicTacToe"
            )

            Column(modifier = Modifier.constrainAs(textGroup) {
                top.linkTo(parent.top)
                bottom.linkTo(button.top)
                start.linkTo(parent.start)
                end.linkTo(parent.end)
            }) {

                val playerName = viewModel.playerName.collectAsState()
                TextField(
                    label = {
                        Text(text = "Player Name")
                    },
                    modifier = Modifier.fillMaxWidth(),
                    value = playerName.value, onValueChange = {
                        viewModel.playerName.value = it
                    })

                Spacer(modifier = Modifier.height(16.dp))

                val roomId = viewModel.roomId.collectAsState()
                TextField(
                    label = {
                        Text(text = "Room Id")
                    },
                    modifier = Modifier.fillMaxWidth(),
                    value = roomId.value, onValueChange = {
                        viewModel.roomId.value = it
                    })
            }

            FloatingActionButton(
                modifier = Modifier
                    .constrainAs(button) {
                        end.linkTo(parent.end)
                        bottom.linkTo(parent.bottom)
                    },
                onClick = {
                    if (viewModel.playerName.value.isNotEmpty() && viewModel.roomId.value.isNotEmpty()) {
                        viewModel.play()
                    } else {
                        Toast.makeText(
                            context,
                            "Please Enter your Name and Room Id",
                            Toast.LENGTH_LONG
                        )
                            .show()
                    }
                }) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_baseline_play_arrow_24),
                    contentDescription = "Play Button"
                )
            }
        }
    }
}

@Preview
@Composable
fun GettingStartedComposablePreview() {
    Scaffold(
        modifier = Modifier.fillMaxSize()
    ) {
        ConstraintLayout(
            modifier = Modifier
                .fillMaxSize()
                .padding(horizontal = 16.dp, vertical = 32.dp)
        ) {
            val (button, textGroup, title) = createRefs()

            Text(
                modifier = Modifier.constrainAs(title) {
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                    bottom.linkTo(textGroup.top)
                },
                text = "TicTacToe"
            )

            Column(modifier = Modifier.constrainAs(textGroup) {
                top.linkTo(parent.top)
                bottom.linkTo(button.top)
                start.linkTo(parent.start)
                end.linkTo(parent.end)
            }) {
                TextField(
                    label = {
                        Text(text = "Player Name")
                    },
                    modifier = Modifier.fillMaxWidth(),
                    value = "", onValueChange = {

                    })

                Spacer(modifier = Modifier.height(16.dp))

                TextField(
                    label = {
                        Text(text = "Room Id")
                    },
                    modifier = Modifier.fillMaxWidth(),
                    value = "", onValueChange = {

                    })
            }

            FloatingActionButton(
                modifier = Modifier
                    .constrainAs(button) {
                        end.linkTo(parent.end)
                        bottom.linkTo(parent.bottom)
                    },
                onClick = { /*TODO*/ }) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_baseline_play_arrow_24),
                    contentDescription = "Play Button"
                )
            }
        }
    }
}