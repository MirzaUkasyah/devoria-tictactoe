package com.lgd.tictactoe.core.widget

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.lgd.tictactoe.R

@Composable
fun SquareComposable(
    value: Int,
    modifier: Modifier = Modifier
) {
    Box(
        modifier = modifier
            .aspectRatio(1 / 1f)
            .border(width = 2.dp, color = Color.Black),
        contentAlignment = Alignment.Center
    ) {
        if (value == 1)
            Icon(
                painter = painterResource(id = R.drawable.ic_outline_circle_24),
                contentDescription = "Square",
                Modifier
                    .aspectRatio(1 / 1f)
                    .fillMaxWidth()
                    .padding(2.dp)
            )

        if (value == 2)
            Icon(
                painter = painterResource(id = R.drawable.ic_baseline_close_24),
                contentDescription = "Square",
                Modifier
                    .aspectRatio(1 / 1f)
                    .fillMaxWidth()
                    .padding(2.dp)
            )
    }
}

@Preview
@Composable
fun SquareComposablePreview() {
    Box(
        modifier = Modifier
            .aspectRatio(1 / 1f)
            .fillMaxWidth()
            .border(width = 2.dp, color = Color.Black),
        contentAlignment = Alignment.Center
    ) {
        Icon(
            painter = painterResource(id = R.drawable.ic_baseline_close_24),
            contentDescription = "Square",
            Modifier
                .aspectRatio(1 / 1f)
                .fillMaxWidth()
                .padding(2.dp)
        )
    }
}