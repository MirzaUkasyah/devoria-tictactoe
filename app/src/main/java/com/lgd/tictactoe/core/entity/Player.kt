package com.lgd.tictactoe.core.entity

data class Player(
    val name: String? = "",
    val number: Long? = 0,
)
