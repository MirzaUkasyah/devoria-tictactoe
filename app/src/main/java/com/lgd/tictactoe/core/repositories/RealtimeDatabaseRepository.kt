package com.lgd.tictactoe.core.repositories

import android.util.Log
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.getField
import com.lgd.tictactoe.core.entity.Player
import com.lgd.tictactoe.core.entity.Room
import com.lgd.tictactoe.core.interfaces.ResultState
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.flow.callbackFlow

class RealtimeDatabaseRepository(
    private val firebaseDatabase: FirebaseFirestore
) {

    fun setData(
        roomId: String,
        players: List<Player>,
        gameState: List<Int>,
        activePlayer: Int,
        message: String
    ) {
        firebaseDatabase.document("rooms/$roomId").set(
            hashMapOf(
                "roomId" to roomId,
                "players" to players,
                "gameState" to gameState,
                "activePlayer" to activePlayer,
                "status" to message,
            )
        )
    }

    fun addPlayer(
        roomId: String,
        players: List<Player>,
    ) {
        firebaseDatabase.document("rooms/$roomId").update("players", players)
    }

    fun updateGameState(roomId: String, data: List<Int>) {
        firebaseDatabase.document("rooms/$roomId").update("gameState", data)
    }

    fun updateActivePlayer(roomId: String, activePlayer: Int) {
        firebaseDatabase.document("rooms/$roomId").update("activePlayer", activePlayer)
    }

    fun updateGameStatus(roomId: String, message: String) {
        firebaseDatabase.document("rooms/$roomId").update("status", message)
    }


    @ExperimentalCoroutinesApi
    fun retrieveGameData(roomId: String) = callbackFlow {
        firebaseDatabase.document("rooms/$roomId").addSnapshotListener { value, error ->
            if (error != null) {
                this@callbackFlow.trySendBlocking(ResultState.Error(error))
            }

            if (value != null && value.exists()) {
                val playerList = value.get("players") as List<Map<String, Any>>

                val room = Room(
                    players = playerList.map {
                        Player(
                            name = it["name"] as String?,
                            number = it["number"] as Long?
                        )
                    },
                    gameState = value.get("gameState") as List<Int>,
                    activePlayer = (value.get("activePlayer") as Long).toInt(),
                    status = value.get("status") as String
                )

                if (room.players.size == 2) {
                    this@callbackFlow.trySendBlocking(ResultState.Success(room))
                } else {
                    this@callbackFlow.trySendBlocking(ResultState.Loading)
                }
            }
        }

        awaitClose { }
    }

    @ExperimentalCoroutinesApi
    fun retrieveRoomList(roomId: String) = callbackFlow {
        firebaseDatabase.document("rooms/$roomId").get().addOnSuccessListener {
            Log.e("Snapshot", it.toString())
            this@callbackFlow.trySendBlocking(ResultState.Success(it))
        }.addOnFailureListener {
            this@callbackFlow.trySendBlocking(ResultState.Error(it))
        }
        awaitClose { }
    }
}