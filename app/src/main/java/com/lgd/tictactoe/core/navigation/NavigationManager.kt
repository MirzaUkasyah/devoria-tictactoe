package com.lgd.tictactoe.core.navigation

import com.lgd.tictactoe.core.interfaces.NavigationCommand
import com.lgd.tictactoe.core.navigation.Directions.Default
import kotlinx.coroutines.flow.MutableStateFlow

class NavigationManager {
    var commands = MutableStateFlow(Default)

    fun navigate(directions: NavigationCommand) {
        commands.value = directions
    }
}