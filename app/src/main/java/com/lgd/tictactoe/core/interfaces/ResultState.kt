package com.lgd.tictactoe.core.interfaces

sealed class ResultState {

    object Loading : ResultState()

    data class Success<T>(val data: T?) : ResultState()

    data class Error(val throwable: Throwable) : ResultState()
}