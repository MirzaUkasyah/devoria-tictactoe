package com.lgd.tictactoe.core.navigation

import androidx.navigation.NamedNavArgument
import com.lgd.tictactoe.core.interfaces.NavigationCommand

object Directions {
    val Default = object : NavigationCommand {
        override val arguments = emptyList<NamedNavArgument>()
        override val destination = ""
    }

    val Home = object : NavigationCommand {
        override val arguments = emptyList<NamedNavArgument>()

        override val destination = "home"
    }

    val Play = object : NavigationCommand {
        override val arguments = emptyList<NamedNavArgument>()
        override val destination = "play"
    }
}