package com.lgd.tictactoe.core.entity

data class Room(
    val gameState: List<Int>,
    val activePlayer: Int,
    val status: String,
    val players: List<Player>
)
